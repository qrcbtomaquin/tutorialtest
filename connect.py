import cx_Oracle
import cx_Oracle as ora

try: 
    dsnStr = ora.makedsn(host="localhost", port="1521", sid="orcl")
    conn = ora.connect(mode=cx_Oracle.SYSDBA, user="SYS", password="DVSYS", dsn=dsnStr)
except Exception as error:
    print("Error connection:", error)

else:
#Inser Data
#    try:
#        cur = conn.cursor()
#        sql_insert = """ INSERT INTO student_details VALUES ('Acerado', 'Risty', '25')"""
#        cur.execute(sql_insert)
#    except Exception as error:
#        print("Error connection:", error)
#    else:
#        print("Data Inserted!")
#        conn.commit()
#fetch Data
    try:
        cur = conn.cursor()
        sql_data = """ SELECT * FROM student_details"""
        cur.execute(sql_data)
        data = cur.fetchall()
        print(data)
    except Exception as error:
        print("Error connection:", error)
        
finally:
    cur.close()
    conn.close()


#sql_create = """
#Create table student_details(
#	Firstname VARCHAR(200),
#	Lastname VARCHAR(200),
#	Age NUMBER
#)
#"""
#cur.execute(sql_create)

